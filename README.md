# Projet_CRPQ_Equipe4
## Projet API

Projet de consultation de base de données pour les étudiants de Technique policière.

Dans le cadre du cours de Projet 2 - 4D1, le premier projet à réaliser ,dans le cadre de ce cours, est de créer une application capable de répliquer l'outil de questionnement de base de données à la disposition d'un agent de l'ordre lors de ses interventions.

## Cadre d'utilisation
Cette application servira comme outil de formation au étudiants en technique policière. Il habituera l'élève à:
-Manipuler une carte d'identité lors de mises en situations
-Se familiarisé avec les informations récoltés lors d'une altercation
-Utilisé un outil de recherche de base de données 

## Colaboration
Ce projet est réalisé par: David Déchaine, Ryma Merrouchi, Charles-Étienne Doucet et Dan Bagalwa
***Le PO : Marc Levasseur
***Le Srum Master : David Déchaine

## Tests
Afin de completer les tests unitaires il faut lancer la commande : npm test
Le port utiliser pour lancer le serveur est le 3000 et la commande script est : ***npm run serve
Afin d'utiliser le linter sur tout les fichiers la commande a taper est : ***npm run lint
Afin d'utiliser le linter sur tout les fichiers et corriger les erreurs la commande a taper est : ***npm run fix
Afin d'utiliser l'API Veuillez installer toutes les dependances avec la commande : ***npm i

